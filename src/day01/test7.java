package day01;

public class test7 {
    /*
    定义一个日期 MyDate类。
    属性：year ：年、month ：月、day ：日
    构造方法：满参构造方法
    成员方法：get/set 方法、showDate 方法：打印日期。isBi 方法：判断当前日期是否是闰年
    定义测试类，创建 MyDate对象，并测试。
    */
    public static void main(String[] args) {
        MyDate myDate = new MyDate(2022,7,7);
        myDate.showDate();
        myDate.isBi(2021);
    }
}
