package day01;

import java.util.Scanner;

public class test4 {
    //题目：一个5位数，判断它是不是回文数。即12321是回文数，个位与万位相同，十位与千位相同。
    public static void main(String[] args) {
        System.out.println("请输入一个五位数:");
        Scanner scanner = new Scanner(System.in);
        int m;
        m = scanner.nextInt();
        while(m<10000 || m>99999) {
            System.out.println("请重新输入一个五位数:");
            m = scanner.nextInt();
        }
        int m1,m2,m4,m5;
        m1 = m%10;
        m2 = m/10%10;
        m4 = m/1000%10;
        m5 = m/10000;
        if(m1 == m5 && m2 == m4) System.out.println("这是一个回文数");
        else System.out.println("这不是一个回文数");
    }
}
