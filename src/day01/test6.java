package day01;

public class test6 {
    /*
    定义一个圆形 Circle类。
    属性：r ：半径
    构造方法：无参构造方法、满参构造方法
    成员方法：get/set 方法、showArea 方法：打印圆形面积、showPerimeter 方法：打印圆形周长
    定义测试类，创建 Circle对象，并测试。
    */
    public static void main(String[] args) {
        Circle circle = new Circle();
        circle.setR(3);
        circle.showArea();
        circle.showPerimeter();
    }
}

