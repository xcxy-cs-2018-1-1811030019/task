package day01;

public class test3 {
    //题目：求1+2!+3!+...+20!的和
    public static void main(String[] args) {
        int sum = 0;
        for(int i = 1;i<=20;i++){
            sum = sum + func(i);
        }
        System.out.println("和是"+sum);
    }
    static int func(int n){
        if(n>1){
            n = n*func(n-1);
        }else{
            n = 1;
        }
        return n;
    }
}
