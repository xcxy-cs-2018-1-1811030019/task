package day01;

import java.util.Scanner;

public class test1 {
    //    输入两个正整数m和n，求其最大公约数和最小公倍数
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入一个正整数m:");
        int m = scanner.nextInt();
        System.out.println("请输入一个正整数n:");
        int n = scanner.nextInt();
        if(m>n) {
            m = n+m;
            n = m - n;
            m = m - n;
        }

        int max = 1;
        for(int i = max;i<=m;i++){
            if(n%i==0 && m%i==0) max = i;
        }
        System.out.println("最大公约数是"+max);

        int min;
        min = max*(m/max)*(n/max);
        System.out.println("最小公倍数是"+min);
    }

}
