package day01;

public class test2 {
    public static void main(String[] args) {
        //打印出所有的 "水仙花数 "，所谓 "水仙花数 "是指一个三位数，其各位数字立方和等于该数本身。例如：153是一个 "水仙花数 "，因为153=1的三次方＋5的三次方＋3的三次方。
        int x,y,z;
        for(int i = 100;i<1000;i++){
            x = i%10;
            y = i/10%10;
            z = i/100;
            if(x*x*x+y*y*y+z*z*z==i) System.out.println(i);
        }
    }
}
