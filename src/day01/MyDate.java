package day01;

public class MyDate {
    private int year;
    private int month;
    private int day;
    public MyDate(int year,int month,int day){
        this.year = year;
        this.month = month;
        this.day = day;
    }
    public int getYear() {
        return year;
    }
    public int getMonth() {
        return month;
    }
    public int getDay() {
        return day;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public void setMonth(int month) {
        this.month = month;
    }
    public void setDay(int day) {
        this.day = day;
    }
    public void showDate(){
        System.out.println("日期是："+this.year+"年"+this.month+"月"+this.day+"日");
    }
    public void isBi(int year){
        if((year%4==0&&year%100!=0)||year%400==0) System.out.println(year+"年是闰年");
        else System.out.println(year+"年不是闰年");
    }
}
